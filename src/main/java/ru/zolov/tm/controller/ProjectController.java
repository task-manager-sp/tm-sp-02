package ru.zolov.tm.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.service.ProjectService;

@Controller
public class ProjectController {

  @Autowired ProjectService projectService;

  @GetMapping("/projects") public ModelAndView getProjects() {
    @NotNull List<Project> projects = projectService.findAll();
    ModelAndView model = new ModelAndView();
    model.addObject("projects", projects);
    model.setViewName("project-list");
    return model;
  }

  @GetMapping(value = "/create") public ModelAndView createProjectGet(
      @ModelAttribute("project") Project project,
      BindingResult result
  ) {
    ModelAndView model = new ModelAndView();
    model.setViewName("create-project");
    return model;
  }

  @PostMapping(value = "/create") public ModelAndView createProjectPost(
      @ModelAttribute("name") String name,
      @ModelAttribute("description") String description,
      BindingResult result
  ) {
    ModelAndView model = new ModelAndView();
    Project project = new Project();
    project.setName(name);
    project.setDescription(description);
    projectService.create(name, description);
    model.setViewName("redirect:/projects");
    return model;
  }

  @GetMapping(value = "/edit/{id}") public ModelAndView editProjectGet(@PathVariable("id") final String id) {
    ModelAndView model = new ModelAndView();
    Project project = projectService.findById(id);
    model.addObject("project", project);
    model.setViewName("edit-project");
    return model;
  }

  @PostMapping(value = "edit") public ModelAndView editProjectPost(@ModelAttribute("project") final Project project) {
    ModelAndView model = new ModelAndView();
    projectService.update(project.getId(), project.getName(), project.getDescription());
    model.setViewName("redirect:/projects");
    return model;
  }

  @GetMapping(value = "/delete/{id}") public ModelAndView deleteProject(@PathVariable("id") String id) {
    ModelAndView model = new ModelAndView();
    projectService.removeById(id);
    model.setViewName("redirect:/projects");
    return model;
  }

  @PostMapping("/search") public ModelAndView searchProject(
      @ModelAttribute(name = "name") String string,
      BindingResult binding
  ) {
    ModelAndView model = new ModelAndView();
    List<Project> list = projectService.findProject(string);
    model.addObject("result", list);
    model.setViewName("search");
    return model;
  }

  @InitBinder public void initBinder(WebDataBinder webDataBinder) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    dateFormat.setLenient(false);
    webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
  }
}
