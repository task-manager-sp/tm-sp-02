package ru.zolov.tm.api;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.zolov.tm.entity.Project;

@Repository
public interface IProjectRepository extends CrudRepository<Project, String> {

  List<Project> findAll();

  List<Project> findProjectsByNameContainsOrDescriptionContains(
      final @NotNull String name,
      final @NotNull String description
  );
}
