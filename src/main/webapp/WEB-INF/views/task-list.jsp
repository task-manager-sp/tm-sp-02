<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: zolov
  Date: 11.02.2020
  Time: 18:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <title>Task Manager</title>
</head>

<body>
<%@ include file="topbar.jsp" %>
<div class="container-fluid">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Task ID</th>
            <th>Project ID</th>
            <th>Name</th>
            <th>Status</th>
            <th>Description</th>
            <th>Created</th>
            <th>Begin</th>
            <th>End</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${tasks}" var="task">
            <tr>
                <td>${task.id}</td>
                <td>${task.projectId}</td>
                <td>${task.name}</td>
                <td>${task.status}</td>
                <td>${task.description}</td>
                <td>${task.dateOfCreate}</td>
                <td>${task.dateOfStart}</td>
                <td>${task.dateOfFinish}</td>
                <td>
                <td>
                    <div class="btn-group" role="group" aria-label="actions">
                        <a class="btn btn-primary btn-sm" href="/task-edit-get/${task.id}">edit</a>
                        <a class="btn btn-primary btn-sm" href="/task-delete/${task.id}">delete</a>
                    </div>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <br><br>
    <form action="/task-create/${projectId}">
        <button class="btn btn-primary btn-lg btn-block" type="submit"><b>New task</b></button>
    </form>
</div>
</body>
</html>